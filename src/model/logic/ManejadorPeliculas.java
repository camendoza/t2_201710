package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;


import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;

	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(archivoPeliculas));
			String linea = br.readLine();
			misPeliculas = new ListaEncadenada<>();
			linea = br.readLine();
			int ind = 1;

			try {
				while (linea != null && ind < 9125) {
					linea = br.readLine();
					if (linea.indexOf("\"") == -1) {
						String casillas[];
						casillas = linea.split(",");
						String titleAndYear = casillas[1];
						String prueba[] = titleAndYear.split("\\(");
						String title;
						String year;
						if (prueba.length == 1) {
							title = titleAndYear;
							year = "0";
						} else if (prueba.length == 2) {
							title = titleAndYear.split("\\(")[0];
							year = titleAndYear.split("\\(")[1];
						} else if (prueba.length == 3) {
							title = titleAndYear.split("\\(")[0] + "(" + titleAndYear.split("\\(")[1];
							year = titleAndYear.split("\\(")[2];
						} else if (prueba.length == 4) {
							title = titleAndYear.split("\\(")[0] + "(" + titleAndYear.split("\\(")[1] + "("
									+ titleAndYear.split("\\(")[2];
							year = titleAndYear.split("\\(")[3];
						} else {
							title = titleAndYear.split("\\(")[0] + "(" + titleAndYear.split("\\(")[1] + "("
									+ titleAndYear.split("\\(")[2] + "(" + titleAndYear.split("\\(")[3];
							year = titleAndYear.split("\\(")[4];
						}
						String genres = casillas[2];
						String[] listaGeneros = genres.split("|");

						VOPelicula pelicula = new VOPelicula();

						ListaEncadenada<String> listGenres = new ListaEncadenada<String>();
						for (int i = 0; i < listaGeneros.length; i++) {
							listGenres.agregarElementoFinal(listaGeneros[i]);
						}
						pelicula.setTitulo(title);
						pelicula.setAgnoPublicacion(Integer.parseInt(year.split("\\)")[0].split("-")[0]));
						pelicula.setGenerosAsociados(listGenres);
						misPeliculas.agregarElementoFinal(pelicula);
						ind++;
					} else {
						String lineas[] = linea.split("\"");
						String titleAndYear;
						String genres;
						if (lineas.length == 3) {
							titleAndYear = lineas[1];
							genres = lineas[2].split(",")[1];
						} else if (lineas.length == 5) {
							titleAndYear = lineas[1] + "\"" + lineas[2] + lineas[3];
							genres = lineas[4].split(",")[1];
						} else {
							titleAndYear = lineas[1] + lineas[2] + "\"" + lineas[3] + "\"" + lineas[4] + lineas[5];
							genres = lineas[6].split(",")[1];
						}
						String prueba[] = titleAndYear.split("\\(");
						String title;
						String year;
						if (prueba.length == 1) {
							title = titleAndYear;
							year = "0";
						} else if (prueba.length == 2) {
							title = titleAndYear.split("\\(")[0];
							year = titleAndYear.split("\\(")[1].split("\\)")[0];
						} else if (prueba.length == 3) {
							title = titleAndYear.split("\\(")[0] + "(" + titleAndYear.split("\\(")[1];
							year = titleAndYear.split("\\(")[2].split("\\)")[0];
						} else if (prueba.length == 4) {
							title = titleAndYear.split("\\(")[0] + "(" + titleAndYear.split("\\(")[1] + "("
									+ titleAndYear.split("\\(")[2];
							year = titleAndYear.split("\\(")[3].split("\\)")[0];
						} else {
							title = titleAndYear.split("\\(")[0] + "(" + titleAndYear.split("\\(")[1] + "("
									+ titleAndYear.split("\\(")[2] + "(" + titleAndYear.split("\\(")[3];
							year = titleAndYear.split("\\(")[4].split("\\)")[0];
						}
						String[] listaGeneros = genres.split("|");

						VOPelicula pelicula = new VOPelicula();

						ListaEncadenada<String> listGenres = new ListaEncadenada<String>();
						for (int i = 0; i < listaGeneros.length; i++) {
							listGenres.agregarElementoFinal(listaGeneros[i]);
						}
						pelicula.setTitulo(title);
						pelicula.setAgnoPublicacion(Integer.parseInt(year.split("-")[0]));
						pelicula.setGenerosAsociados(listGenres);
						misPeliculas.agregarElementoFinal(pelicula);
						ind++;
					}

				}

			} catch (Exception e) {
				System.out.println("Error en pel�cula: " + linea.split(",")[0]);
				System.out.println(linea);
				System.out.println(e.getMessage());
				System.out.println("ind: " + ind);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage() + "Fallo en el inicio");
		}

		ListaEncadenada<VOPelicula> misPeliculasOrdenadas = (ListaEncadenada<VOPelicula>) ordenarListaDeAnios(
				misPeliculas);
		
		ListaDobleEncadenada<VOAgnoPelicula> futurasPeliculasAgno = new ListaDobleEncadenada<>();
		ListaEncadenada<VOPelicula> temp = new ListaEncadenada<>();
		temp.agregarElementoFinal(misPeliculasOrdenadas.darElemento(0));
		for (int i = 0; i < misPeliculasOrdenadas.darNumeroElementos() - 1; i++) {
			VOPelicula act = misPeliculasOrdenadas.darElemento(i);
			VOPelicula sig = misPeliculasOrdenadas.darElemento(i + 1);
			if (act.getAgnoPublicacion() == sig.getAgnoPublicacion())
				temp.agregarElementoFinal(sig);
			else {
				VOAgnoPelicula aAgregar = new VOAgnoPelicula();
				aAgregar.setAgno(act.getAgnoPublicacion());
				aAgregar.setPeliculas(temp);
				futurasPeliculasAgno.agregarElementoFinal(aAgregar);
				temp = new ListaEncadenada<>();
			}
		}
		peliculasAgno = futurasPeliculasAgno;
	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {

		ListaEncadenada<VOPelicula> resp = new ListaEncadenada<VOPelicula>();

		boolean termino = false;
		while (termino == false) {
			for (int i = 0; i < misPeliculas.darNumeroElementos(); i++) {
				VOPelicula elemAct = misPeliculas.darElemento(i);
				String titulo = elemAct.getTitulo();
				if (titulo.toLowerCase().indexOf(busqueda.toLowerCase()) != -1) {
					resp.agregarElementoFinal(elemAct);
				}

			}
			termino = true;
		}
		return resp;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		// TODO Auto-generated method stub
		ListaEncadenada<VOPelicula> rta = new ListaEncadenada<VOPelicula>();
		boolean end = false;
		while (peliculasAgno != null && !end) {
			VOAgnoPelicula act = peliculasAgno.darElementoPosicionActual();
			if (act.getAgno() == agno) {
				rta = (ListaEncadenada<VOPelicula>) act.getPeliculas();
				end = true;
			} else {
				peliculasAgno.avanzarSiguientePosicion();
			}
		}
		return rta;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
		peliculasAgno.avanzarSiguientePosicion();
		VOAgnoPelicula rta = peliculasAgno.darElementoPosicionActual();
		return rta;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		peliculasAgno.retrocederPosicionAnterior();
		VOAgnoPelicula rta = peliculasAgno.darElementoPosicionActual();
		return rta;
	}

	public ListaEncadenada<VOPelicula> ordenarListaDeAnios(ILista<VOPelicula> lista) {

		ArrayList<VOPelicula> tem = new ArrayList<VOPelicula>();
		ListaEncadenada<VOPelicula> temp2 = new ListaEncadenada<VOPelicula>();

		while (lista.avanzarSiguientePosicion() != false) {
			tem.add(lista.darElementoPosicionActual());
			lista.avanzarSiguientePosicion();
		}
		for (int i = 0; i < (tem.size() - 1); i++) {
			for (int j = i + 1; j < tem.size(); j++) {

				if (tem.get(i).getAgnoPublicacion() > tem.get(j).getAgnoPublicacion()) {

					VOPelicula vAuxiliar = tem.get(i);
					tem.set(i, tem.get(j));
					tem.set(j, vAuxiliar);

				}
			}
		}
		for (int i = 0; i < tem.size(); i++) {
			temp2.agregarElementoFinal(tem.get(i));
		}
		lista = temp2;

		return temp2;

	}

}
