package t2_201710;

import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;

public class ListaDobleEncadenadaTest extends TestCase {
	
	private ListaDobleEncadenada<String> listaDoble;
	
	public void setupEscenario1()
	{
		listaDoble = new ListaDobleEncadenada();
	}
	public void setupEscenario2()
	{
		setupEscenario1();
		listaDoble.agregarElementoFinal("Primer elemento");
		listaDoble.agregarElementoFinal("Segundo elemento");
		listaDoble.agregarElementoFinal("Tercer elemento");
	}
	public void setupEscenario3()
	{
		setupEscenario2();
		listaDoble.agregarElementoFinal("Cuarto elemento");
		listaDoble.agregarElementoFinal("Quinto elemento");
		listaDoble.agregarElementoFinal("Sexto elemento");
	}
	public void setupEscenario4()
	{
		setupEscenario3();
		listaDoble.avanzarSiguientePosicion();
		listaDoble.avanzarSiguientePosicion();
		listaDoble.avanzarSiguientePosicion();
		listaDoble.avanzarSiguientePosicion();
	}
	public void testAgregarElementoFinal()
	{
		setupEscenario3();
		listaDoble.agregarElementoFinal("Septimo elemento");
		assertEquals("El elemento no se agreg� en la posici�n final", "Septimo elemento", listaDoble.darElemento(6));
	}
	public void testDarElemento()
	{
		setupEscenario2();
		assertEquals("El elemento no coincide", "Segundo elemento", listaDoble.darElemento(1));
	}
	public void testDarNumeroElementos()
	{
		setupEscenario1();
		assertEquals("La lista deber�a estar vacia", 0, listaDoble.darNumeroElementos());
		
		setupEscenario3();
		assertEquals("La lista deber�a tener 6 elementos", 6,  listaDoble.darNumeroElementos());
	}
	public void testDarElementoPosicionActual()
	{
		setupEscenario1();
		assertEquals("La lista no deber�a tener elementos", null, listaDoble.darElementoPosicionActual());
		setupEscenario2();
		listaDoble.avanzarSiguientePosicion();
		listaDoble.avanzarSiguientePosicion();
		assertEquals("La posici�n actual no coincide", "Tercer elemento", listaDoble.darElementoPosicionActual());
	}
	public void testAvanzarSiguientePosicion()
	{
		setupEscenario3();
		listaDoble.avanzarSiguientePosicion();
		listaDoble.avanzarSiguientePosicion();
		listaDoble.avanzarSiguientePosicion();
		assertTrue("Deber�a avanzar a la posici�n siguiente", listaDoble.avanzarSiguientePosicion());
		assertEquals("La posici�n actual no coincide", "Quinto elemento", listaDoble.darElementoPosicionActual());
	}
	public void testRetrocederPosicionAnterior()
	{
		setupEscenario4();
		listaDoble.retrocederPosicionAnterior();
		listaDoble.retrocederPosicionAnterior();
		assertTrue("Deber�a retroceder a la posici�n anterior", listaDoble.retrocederPosicionAnterior());
		assertEquals("La posici�n actual no coincide", "Segundo elemento", listaDoble.darElementoPosicionActual());
		
	}
	
}
