package t2_201710;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

public class ListaEncadenadaTest extends TestCase
{

	private ListaEncadenada<String> listaEncadenada;
	
	public void setupEscenario1 ()
	{
		listaEncadenada = new ListaEncadenada();
	}
	public void setupEscenario2()
	{
		setupEscenario1();
		listaEncadenada.agregarElementoFinal("Primer elemento");
		listaEncadenada.agregarElementoFinal("Segundo elemento");
		listaEncadenada.agregarElementoFinal("Tercer elemento");
	}
	
	public void setupEscenario3 ()
	{
	  setupEscenario2();
	  listaEncadenada.agregarElementoFinal("Cuarto elemento");
	  listaEncadenada.agregarElementoFinal("Quinto elemento");
      listaEncadenada.agregarElementoFinal("Sexto elemento");
	  
	}
	
	public void setupEscenario4()
	{
		setupEscenario3();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
	}
	public void testAgregarElementoFinal()
	{
		setupEscenario2();
		assertEquals( "El elemento no se agrego en la ultima posicion", "Tercer elemento", listaEncadenada.darElemento(2) );
	}
	
	public void testDarElemento()
	{
		setupEscenario2();
		assertEquals( "El elemento no corresponde", "Primer elemento", listaEncadenada.darElemento(0) );
		
	}
	
	public void testDarNumeroElementos()
	{
		setupEscenario2();
		assertEquals( "El numero de elementos no corresponde", 3, listaEncadenada.darNumeroElementos() );
	}
	
	public void testAvanzarSiguientePosicion()
	{
		setupEscenario3();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		assertEquals( "No se avanzo correctamente a la siguiente posicion", "Cuarto elemento", listaEncadenada.darElementoPosicionActual() );
		
	}
	public void testRetrocederPosicionAnterior()
	{
		setupEscenario4();
		listaEncadenada.retrocederPosicionAnterior();
		listaEncadenada.retrocederPosicionAnterior();
		assertEquals( "No se avanzo correctamente a la siguiente posicion", "Segundo elemento", listaEncadenada.darElementoPosicionActual() );
		
		
	}
	
	public void testDarElementoPosicionActual()
	{
		setupEscenario2();
		assertEquals( "No se avanzo correctamente a la posicion anterior", "Primer elemento", listaEncadenada.darElementoPosicionActual() );
		
	}
	
	
}
